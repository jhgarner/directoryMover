#! /usr/bin/env python3
# Finds projects based on the config.json file in your ~/directories/ folder.
# All discovered projects get automatically backed up if their repos are
# unclean.

from os import listdir, makedirs
import datetime
import json
from os.path import isfile, join, isdir
from subprocess import run
from subprocess import PIPE
from subprocess import CalledProcessError
from pathlib import Path
from requests import get
from directoryMover.lock import Lock
from platform import node
from sys import argv


home = argv[1] if len(argv) >= 2 else str(Path.home())
dRepo = f"/directories/"
hostName = node()

found = {}

def cmd(command, file = ".", fail=True):
    if isinstance(command, str):
        return run(command.split(), cwd=f"{home}/{file}", stdout=PIPE, stderr=PIPE, universal_newlines=True, check=fail)
    else:
        return run(command, cwd=f"{home}/{file}", stdout=PIPE, stderr=PIPE, universal_newlines=True, check=fail)

def getUntracked(file, l):
    diff = cmd("git diff -U0 --binary", file, False).stdout
    for untracked in cmd("git ls-files --others --exclude-standard", file, False).stdout.split("\n")[:-1]:
        try:
            diff += "\n" + cmd("git diff -U0 --binary --no-index /dev/null".split() + [untracked], file, False).stdout

        except UnicodeDecodeError:
            pass
    if len(diff) <= 5 * 1024 * 1024:
        return diff
    else:
        l.fail(f"Change set too large in {file}.")
        return ""

def createTree(l):
    try:
        with open(f"{home}/{dRepo}/{hostName}.json", "r") as tree:
            structure = json.load(tree)

            for location in structure:
                if isdir(f"{home}/{location}"):
                    for remote in structure[location]:
                        cmd(["git", "remote", "remove", remote], location)
                        cmd(["git", "remote", "add", remote, structure[location][remote]], location)
                else:
                    makedirs(f"{home}/{location}")
                    origin = structure[location]["origin"]
                    cmd(["git", "clone", origin, "."], location)
                    for remote in structure[location]:
                        if remote != "origin":
                            cmd(["git", "remote", "add", remote, structure[location][remote]], location)
    except OSError:
        pass
            

def getUrl(file, l):
    remotes = {}
    for remote in cmd("git remote show", file).stdout.split("\n")[:-1]:
        remotes[remote] = cmd("git remote get-url " + remote, file).stdout[:-1]
    if remotes == {}:
        try:
            gotUrl = cmd("git config --get branch.master.remote", file)
            # Update the origin to whatever Gitlab had
            cmd("git remote add origin " + gotUrl.stdout, file)
            remotes["origin"] = gotUrl.stdout
        except CalledProcessError:
            l.fail("ERROR: Unable to read remote " + file)
    return remotes
    
def writeDiff(file, l):
    diff = getUntracked(file, l)
    # One of these checkouts will work
    cmd(["git", "checkout", "-f", file], dRepo, False)
    cmd(["git", "checkout", "--orphan", file], dRepo, False)
    cmd("git reset", dRepo)
    
    with open(f"{home}/{dRepo}/{hostName}.patch", "w") as fout:
        fout.write(diff)
    cmd(["git", "add", f"{hostName}.patch"], dRepo)
    cmd(f"git commit -m '{str(datetime.datetime.now()).replace(' ', '_')}'", dRepo, False)
    try:
        head = cmd(f"git rev-parse --abbrev-ref HEAD", dRepo, True).stdout
        cmd(f"git push origin {head}", dRepo, True)
    except:
        # It probably failed because of race conditions.
        # Just pull and try again.
        # If it's a real problem, Python will stack overflow and crash.
        head = cmd(f"git rev-parse --abbrev-ref HEAD", dRepo, True).stdout
        cmd(f"git pull origin {head}", dRepo, True)
        writeDiff(file, l)
    result = cmd(["git", "log", "-1", '--pretty=format:%at', f"{home}/{dRepo}/{hostName}.patch"], dRepo).stdout
    lastChanged = datetime.date.fromtimestamp(int(result))
    now = datetime.date.today()
    if (now - lastChanged).days >= 7 and diff != "":
        l.warn("WARNING: Synced changes older than a week " + join(file))


def main():
    files = []

    # This should be safe to do without grabbing the lock, right?
    cmd("git fetch", dRepo)
    with Lock(f"{home}/{dRepo}/.lock", f"{home}/{dRepo}/.message") as l:
        cmd("git checkout master -f", dRepo, False)
        cmd("git pull origin master", dRepo)
        with open(f"{home}/directories/config.json", "r") as fout:
            config = json.load(fout)
            files = config["include"]

        projects = {}
        for file in files:
            if isfile(f"{home}/{file}"):
                l.fail("ERROR: File outside of repo " + join(file))
            elif isdir(f"{home}/{file}/.git"):
                # Found a git repo!
                repo = getUrl(file, l)
                writeDiff(file, l)
                projects[file] = repo
            else: 
                contents = listdir(f"{home}/{file}")
                for content in contents:
                    files.append(f"{file}/{content}")
        cmd("git checkout master -f", dRepo)
        with open(f"{home}/{dRepo}/{hostName}.json", "w") as fout:
            json.dump(projects, fout, sort_keys=True, indent=4)
        cmd(f"git add {hostName}.json", dRepo)
        cmd(f"git commit -m '{str(datetime.datetime.now()).replace(' ', '_')}'", dRepo, False)
        cmd(f"git push origin master", dRepo)


def updateTree():
    cmd("git fetch", dRepo)
    with Lock(f"{home}/{dRepo}/.lock", f"{home}/{dRepo}/.message") as l:
        cmd("git checkout master -f", dRepo, False)
        cmd("git pull origin master", dRepo)
        createTree(l)

def lockRepoManually():
    l = Lock(f"{home}/{dRepo}/.lock", f"/dev/null")
    l.__enter__()
def unlockRepoManually():
    l = Path(f"{home}/{dRepo}/.lock")
    if l.exists() and l.is_dir():
        l.rmdir()
    else:
        print(f"ERROR: Unable to delete path {home}/{dRepo}/.lock")

# TODO After writing this code, I realized it missed a lot of edge cases that
# the user might care about. The best solution seems to be a fully interactive
# UI. I really didn't want to write a UI though so I decided to document how to
# merge the trees instead.
# def mergeTrees():
#     cmd("git fetch", dRepo)
#     with Lock(f"{home}/{dRepo}/.lock", f"{home}/{dRepo}/.message") as l:
#         cmd("git checkout master -f", dRepo, False)
#         cmd("git pull origin master", dRepo)
#         jsonFiles = [f for f in listdir(f"{home}/{dRepo}/") if isfile(join(f"{home}/{dRepo}/", f)) and f.endswith(".json")]
#         for i in range(len(jsonFiles)):
#             print(f"{i}) {jsonFiles[i]}")
#         a, b = input("Enter the numbers (separated by a space) for the two files that should be combined: ").split()
#         with open(f"{home}/{dRepo}/{jsonFiles[a]}.json", "w") as faout:
#             with open(f"{home}/{dRepo}/{jsonFiles[b]}.json", "w") as fbout:
#                 newObj = {}
#                 aobj = json.load(faout)
#                 bobj = json.load(fbout)
#                 for key in aobj:
#                     if key not in bobj or aobj[key] == bobj[key]:
#                         newObj[key] = aobj[key]
#                     else:
#                         print("Conflict detected")
#                         print(f"a) {aobj[key]}")
#                         print(f"b) {bobj[key]}")
#                         print(f"c) Something else")
#                         choice = input(f"Which version do you want to keep (a, b, or c)? ")
#                         if choice == "c":
#                             c = input("Enter the new value: ")
#                             newObj[key] = c
#                         elif choice == "a":
#                             newObj[key] = aobj[key]
#                         else:
#                             newObj[key] = bobj[key]
