from pathlib import Path

class Lock:
    def __init__(self, lockFile, messageFile):
        self.lockFile = lockFile
        self.messageFile = messageFile
        self.result = ""
        self.warnings = ""

    def __enter__(self):
        if Path(self.lockFile).exists():
            print(f"ERROR: Lock file already at: {self.lockFile}")
            exit(1)
        Path(self.lockFile).mkdir()
        Path(self.messageFile).touch()
        return self

    def __exit__(self, type, value, tb):
        if tb is not None:
            self.result += "\n" + str(value)
        Path(self.messageFile).write_text(self.result+"\n"+self.warnings)
        Path(self.lockFile).rmdir()
    def fail(self, message):
        self.result += "\n" + message
    def warn(self, message):
        self.warnings += "\n" + message
