# setup.py
from setuptools import setup, find_packages

setup(
    name='directoryMover',
    version='0.0.1',
    packages=find_packages(),
    entry_points = {
        'console_scripts': ['syncProjects=directoryMover.mapProject:main', 'updateTree=directoryMover.mapProject:updateTree', 'lockSync=directoryMover.mapProject:lockRepoManually', 'unlockSync=directoryMover.mapProject:unlockRepoManually'],
    },
)
