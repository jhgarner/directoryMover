![](logomaybe.png)
# Directory Mover

This project provides an opinionated backup program for files in your home
folder. It has the following main ideas:

1. Rely on Git and Git hosting as the primary way of syncing user content.

2. Yell loudly if the user stores data that isn't being synced.

3. Store data in such a way that users could restore or backup everything by
   hand if needed.

4. Prefer manual intervention over custom heuristics.

This leads to the current (WIP) solution. Setting everything up is really easy
if you use NixOS. On other platforms, it will be harder only because I haven't
created install scripts yet.

# Installation

## Installing on NixOS

Navigate to the CI pipeline for this repo and download the `directoryMover.nix`
file. Copy it into `/etc/nixos/directoryMover.nix`. In your main
`configuration.nix`, import the new file and add `services.directoryMover.enable
= true;` to your config. Once you nix-rebuild, the version from the CI pipeline
will be installed, an hourly systemd timer will be setup, the mapProject.py file
will run on suspend, and new ZSH terminals you open will display any error
messages from the mapProject.py file.

Jump to setting up your home folder for the last part of installation.

### Arch
Run `makepkg -si` in this directory to build and install the package. This
should also setup systemd scripts and a systemd timer that you can enable. The
timer is `directoryMover.timer` and the backup on sleep service is
`directoryMoverSleep.service`. The timer is a user service and the sleep version
is a system service.

### Ubuntu/Others
TODO create packages

## Installing on other OS's

TODO Need to investigate Windows/BSD/Mac

## Setting up your home folder

Create a folder at `~/directories` and initialize it as a git repo with `origin`
pointing somewhere. It's important that whatever origin points to does not
require any password prompts. When your device gets suspended, sshagent will not
be available so you will need an https clone. On my machines, I have origin
hardcoded to use a Gitlab user token for that repository. You should be able to
perform a similar trick on Github. If your device is compromised, then an
attacker will be able to read and write to your `directories` repository.
Because of that, it is important to mark branches as protected so that no one
can force push to them. Depending on what you choose to back up, you might want
to make origin a private repo.

Next, copy the config.json from this project into `~/directories/config.json`
and edit it to include all directories relative to your home folder that you
want backed up and synced. Push that file to the repo and you're done. This
program will now run every hour and will make sure you don't accidentally lose
any work.

TODO write home folder setup script.

# Usage

The commands provided by directory mover are very focused. Most behavior gets
delegated to other programs (particularly `git` and `diff`). In the future I
want to have nice UI's for the actions described here. For now you'll just have
to put up with documentation.

## Running a backup now

Using the command `syncProjects` for that.

## Clone repos from my directory file
Use `updateTree` for that. This will clone any repositories you added to the
directory file manually and will change the remotes to match the file.

There is a race condition around this command between sources of truth. The
`syncProjects` command considers your file system to be the source of truth.
Therefore, it overwrites the directory file to match your file system. The
`updateTree` command treats the directory file as the source of truth and
updates your file system to match it. If you update the directory file in the
repo but don't run `updateTree` before systemd runs `syncProjects`, your changes
to the directory file will be overwritten. If this happens, you'll need to
revert the change made by `sycProjects`.

It's best practice to only modify a directory file on the machine that file
relates to. This way, you can also create a `.lock` file and make sure no race
conditions occur.

A future version of Directory Mover should find a more elegant way to handle
this.

## Combine to directory files

First, create a lock file with `lockSync` and make sure you are on the master
branch.

You should run the command
`diff -DVERSION1 myMachine.json otherMachine.json > merged.json` to generate a
diff between the two files. You can then edit `merged.json` with your favorite
text editor to pick which parts from each to keep. Once you're happy with the
result, you can use `mv merged.json myMachine.json` to replace the old file.

To finish up, you need to commit and push your changes. After that, run
`unlockSync` to remove the lock file.

## Applying a backed up change

First, create a lock file with `lockSync`.

Next, checkout the branch corresponding to the path you want to pull the changed
from. For example, if you want to update files located at `~/Documents/CSCI400`
then you would run `git checkout "Documents/CSCI400"`. Running `ls` should show
you patch files for every machine you're syncing. Pick the patch file you want
to apply then `cd` into the root of the project you're syncing. From there, run
`git apply --unidiff-zero ~/directories/otherMachine.patch`

Git can be pretty picky about the state of the project you're trying to apply
the patch to. If your local version and the version on the other machine have
both diverged, you'll need to manually figure out how exactly to merge things.

Once you've synced the changes, run `unlockSync` and consider committing and
pushing your project. Directory Mover is designed to complement a repository.
The longer you pass around patches without committing the changes, the more
cumbersome it will be to apply patches in the future.

## Find why something went wrong

Directory Mover populates the `.messages` file in your `~/directories` folder
with any errors it encountered. I would recommend adding a hook to echo that
file whenever you launch a terminal. The Nixos module includes code to do that
in zsh.
