#! /usr/bin/env sh

sha=$(nix-prefetch-url --unpack https://gitlab.com/jhgarner/directoryMover/-/archive/$CI_COMMIT_SHA/directoryMover-$CI_COMMIT_SHA.tar.gz)
sed -i "s/(rev)/$CI_COMMIT_SHA/" directoryMover.nix
sed -i "s/(sha256)/$sha/" directoryMover.nix
